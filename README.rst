mf6-dupuit
==========

Scripts and examples to run MODFLOW6 without aquitard layers. This is the Dupuit-Forchheimer assumption: groundwater flows horizontally horizontally in aquifers and vertically in aquitards.
