import datetime

from xmipy import XmiWrapper
import numpy as np


class Simulation:
    """
    This is equivalent to running MODFLOW6 in the command line using the mf6
    executable.
    """

    def __init__(self, wdir: str, name: str):
        # We'll need to uppercase the name, since MODFLOW6 does this internally
        # as well.
        self.modelname = name.upper()
        self.mf6 = XmiWrapper(lib_path="libmf6.dll", working_directory=wdir)
        self.mf6.initialize()
        mxiter_tag = self.mf6.get_var_address("MXITER", "SLN_1")
        self.max_iter = self.mf6.get_value_ptr(mxiter_tag)[0]
        shape = np.zeros(1, dtype=np.int32)
        self.ncell = self.mf6.get_grid_shape(1, shape)[0]

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.finalize()

    def do_iter(self, sol_id: int) -> bool:
        """Execute a single iteration"""
        has_converged = self.mf6.solve(sol_id)
        return has_converged

    def update(self):
        # We cannot set the timestep (yet) in Modflow
        # -> set to the (dummy) value 0.0 for now
        self.mf6.prepare_time_step(0.0)
        self.mf6.prepare_solve(1)
        # Convergence loop
        self.mf6.prepare_solve(1)
        for kiter in range(1, self.max_iter + 1):
            has_converged = self.do_iter(1)
            if has_converged:
                print(f"MF6 converged in {kiter} iterations")
                break
        self.mf6.finalize_solve(1)
        # Finish timestep
        self.mf6.finalize_time_step()
        current_time = self.mf6.get_current_time()
        return current_time

    def get_times(self):
        """Return times"""
        return (
            self.mf6.get_start_time(),
            self.mf6.get_current_time(),
            self.mf6.get_end_time(),
        )

    def run(self):
        start = datetime.datetime.now()

        _, current_time, end_time = self.get_times()
        while current_time < end_time:
            current_time = self.update()

        stop = datetime.datetime.now()
        print(
            f"Elapsed run time: {stop - start} (hours: minutes: seconds)."
            f"Simulation terminated normally."
        )

    def finalize(self):
        self.mf6.finalize()


class DupuitForchheimerSimulation(Simulation):
    """
    This simulation assumes only aquifers are present in the model.
    Furthermore, it assumes the hydraulic resistance values have been written
    in the K33 values of the NodePropertyFlow (NPF) package.
    
    This initializes the model using MODFLOW6's default logic, then overwrites
    the values of the saturated conductance (CONDSAT) array for the vertical
    connections.
    """
    def __init__(self, wdir: str, name: str):
        super().__init__(wdir, name)
        self.set_resistance()
        
    def conductance_index(
        self,
        vertical,
    ):
        """
        We've looked at the upper diagonal half of the connectivity matrix.

        While this means that j is always larger than i, it does not mean that cell 
        i is always overlying cell j; the cell numbering may be arbitrary.
        
        In our convention, the k33 values is interpreted as the resistance to the
        cell BELOW it. Should now a case arise when j > i, but with cell j overlying
        cell i, we should use the k33 value of cell j to set the conductance.
        
        Parameters
        ----------
        vertical: np.ndarray of bool
        
        Returns
        -------
        index: np.ndarray of integer
        """
        mf6 = self.mf6
        modelname = self.modelname
        
        top_tag = mf6.get_var_address("TOP", modelname, "DIS")
        bottom_tag = mf6.get_var_address("BOT", modelname, "DIS")
        ia_tag = mf6.get_var_address("IA", modelname, "CON")
        ja_tag = mf6.get_var_address("JA", modelname, "CON")
        
        top = mf6.get_value_ptr(top_tag)
        bottom = mf6.get_value_ptr(bottom_tag)
        ncell = top.size
        # Collect cell-to-cell connections
        # Python is 0-based, Fortran is 1-based
        # Never use IAUSR and JAUSR if NODEREDUCED: none of the arrays to
        # index into contain data for IDOMAIN < 1.
        ia = mf6.get_value_ptr(ia_tag) - 1
        ja = mf6.get_value_ptr(ja_tag) - 1
        # Convert compressed sparse row (CSR) to row(i) and column(j) numbers:
        n = np.diff(ia)
        i = np.repeat(np.arange(ncell), n)
        j = ja
        # Get the upper diagonal, and only the vertical entries.
        upper = j > i
        i = i[upper][vertical]
        j = j[upper][vertical]
        # Now find out which cell is on top, i or j.
        top_i = top[i]
        bottom_j = bottom[j]
        take_i = top_i >= bottom_j
        take_j = ~take_i
        # Create the index with the appropriate values of i and j.
        index = np.empty(i.size, dtype=np.int32)
        index[take_i] = i[take_i]
        index[take_j] = j[take_j]
        return index
        
    def set_resistance(self):
        mf6 = self.mf6
        modelname = self.modelname

        area_tag = mf6.get_var_address("AREA", modelname, "DIS")
        k33_tag = mf6.get_var_address("K33", modelname, "NPF")
        condsat_tag = mf6.get_var_address("CONDSAT", modelname, "NPF")
        ihc_tag = mf6.get_var_address("IHC", modelname, "CON")
        
        # Grab views on the MODFLOW6 memory:
        area = mf6.get_value_ptr(area_tag)
        k33 = mf6.get_value_ptr(k33_tag)
        self.condsat = mf6.get_value_ptr(condsat_tag)
        ihc = mf6.get_value_ptr(ihc_tag)
        vertical = ihc == 0
        self.new_cond = area / k33
        self.cell_i = self.conductance_index(vertical)
        self.condsat[vertical] = self.new_cond[self.cell_i]
