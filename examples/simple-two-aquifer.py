"""
Dupuit-Forchheimer "hack": simple two aquifer example
=====================================================

In contrast to earlier versions of MODFLOW, MODFLOW6 only officially supports
fully 3D simulations. This means that the number of layers doubles minus one:
before, heads were only computed for aquifers while in MODFLOW6 heads are
computed for both aquifers and aquitards.

This has some downsides: the required amount of RAM increases, and computation
times mildly increase (by roughly 70%). However, MODFLOW6's logic can be
modified relatively easy through its API, provided by ``xmipy`` in Python.
This requires a script utilizing ``xmipy``, and customized MODFLOW6 input.
This example demonstrates both.

In a nutshell, this hack consists of:

* Write MODFLOW6 input with reduced layers, representing the aquifers
  exclusively.
* As we cannot rely on MODFLOW6 to use the aquitard thickness to compute
  aquitard resistance, we assign it ourselves to the K33 entry of the
  NodePropertyFlow package.
* Initialize MODFLOW6, fetch the K33 values from memory, and assign these
  to the saturated conductance (CONDSAT).
* Run the MODFLOW6 simulation to completion.

"""
# %%

import numpy as np
import xarray as xr
import imod

from mf6_dupuit import Simulation, DupuitForchheimerSimulation

# %%
# Creating input
# ==============
#
# As mentioned, the primary issue is to write the resistance of the aquitard
# layers into K33. Unfortunately, there is one more hurdle: in MODFLOW6, all
# layers are assumed to be contiguous in depth for DIS and DISV discretization
# packages. This means that for a layer, the bottom of the overlying layer is
# equal its top. DISU is an exception, and allows separate top and bottom
# values. Consequently, we will write all the model as if it is a DISU model.
#
# We can do with relative easy, by creating a
# ``LowLevelUnstructuredDiscretization`` instance via its ``from_dis`` method.
# Additionally, all packages features a ``to_disu`` method. Otherwise, we
# assemble and write the model as usual.
#
# Model: 3D
# ---------
#
# However, let's start with a small fully 3D benchmark. The model features:
#
# * recharge across the entire top layer;
# * two aquifers of 50 m separated by an aquitard of 10 m;
# * a drain in the center.

nlay = 3
nrow = 150
ncol = 151
shape = (nlay, nrow, ncol)

dx = 10.0
dy = -10.0
xmin = 0.0
xmax = dx * ncol
ymin = 0.0
ymax = abs(dy) * nrow
dims = ("layer", "y", "x")

layer = np.array([1, 2, 3])
y = np.arange(ymax, ymin, dy) + 0.5 * dy
x = np.arange(xmin, xmax, dx) + 0.5 * dx
coords = {"layer": layer, "y": y, "x": x}

idomain = xr.DataArray(np.ones(shape), coords=coords, dims=dims)
bottom = xr.DataArray([40.0, 30.0, 0.0], {"layer": layer}, ("layer",))
top = xr.DataArray(50.0)
icelltype = xr.DataArray([1, 0, 0], {"layer": layer}, ("layer",))
recharge = xr.full_like(idomain.sel(layer=1), 0.001)

# We'll assume a resistance of 10 days, for a ditch of 2 m wide.
conductance = xr.full_like(idomain.sel(layer=1), np.nan)
elevation = xr.full_like(idomain.sel(layer=1), np.nan)
conductance[:, 75] = 20.0
elevation[:, 75] = 42.0

# %%
# For comparison, we set horizontal conductivity of the aquitard to a tiny value.

k = xr.DataArray([10.0, 1.0e-6, 10.0], {"layer": layer}, ("layer",))
k33 = xr.DataArray([10.0, 0.1, 10.0], {"layer": layer}, ("layer",))

gwf_model = imod.mf6.GroundwaterFlowModel()
gwf_model["dis"] = imod.mf6.StructuredDiscretization(
    top=top, bottom=bottom, idomain=idomain.astype(np.int32)
)
gwf_model["drn"] = imod.mf6.Drainage(
    elevation=elevation,
    conductance=conductance,
    print_input=True,
    print_flows=True,
    save_flows=True,
)
gwf_model["ic"] = imod.mf6.InitialConditions(head=45.0)
gwf_model["npf"] = imod.mf6.NodePropertyFlow(
    icelltype=icelltype,
    k=k,
    k33=k33,
)
gwf_model["sto"] = imod.mf6.SpecificStorage(
    specific_storage=1.0e-5,
    specific_yield=0.15,
    transient=False,
    convertible=0,
)
gwf_model["oc"] = imod.mf6.OutputControl(save_head="all", save_budget="all")
gwf_model["rch"] = imod.mf6.Recharge(recharge)

# Attach it to a simulation
simulation = imod.mf6.Modflow6Simulation("model3d")
simulation["model3d"] = gwf_model
# Define solver settings
simulation["solver"] = imod.mf6.Solution(
    print_option="summary",
    csv_output=False,
    no_ptc=True,
    outer_dvclose=1.0e-4,
    outer_maximum=500,
    under_relaxation=None,
    inner_dvclose=1.0e-4,
    inner_rclose=0.001,
    inner_maximum=100,
    linear_acceleration="cg",
    scaling_method=None,
    reordering_method=None,
    relaxation_factor=0.97,
)
simulation.time_discretization(["2020-01-01", "2020-01-02"])

path_3d = imod.util.temporary_directory()
simulation.write(path_3d)

# %%
# Let's run the simulation with our class defined above, and visualize the
# results:

with Simulation(path_3d, "dupuit") as xmi_simulation:
    xmi_simulation.run()

head3d = imod.mf6.open_hds_like(path_3d / "model3d/model3d.hds", idomain)
head3d.isel(time=0, y=75).plot.line(hue="layer")

# %%
# Model: Dupuit-Forchheimerr
# --------------------------
#
# We can now create the reduced model.

nlay = 2
nrow = 150
ncol = 151
shape = (nlay, nrow, ncol)

layer = np.array([1, 2])
y = np.arange(ymax, ymin, dy) + 0.5 * dy
x = np.arange(xmin, xmax, dx) + 0.5 * dx
coords = {"layer": layer, "y": y, "x": x}

shape = (nlay, nrow, ncol)
idomain = xr.DataArray(np.ones(shape), coords=coords, dims=dims)
bottom = idomain * xr.DataArray([40.0, 0.0], {"layer": layer}, ("layer",))
top = idomain * xr.DataArray([50.0, 30.0], {"layer": layer}, ("layer",))
icelltype = idomain * xr.DataArray([1, 0], {"layer": layer}, ("layer",))
recharge = xr.full_like(idomain.sel(layer=1), 0.001)

# We'll assume a resistance of 10 days, for a ditch of 2 m wide.
conductance = xr.full_like(idomain.sel(layer=1), np.nan)
elevation = xr.full_like(idomain.sel(layer=1), np.nan)
conductance[:, 75] = 20.0
elevation[:, 75] = 42.0

# %%
# For comparison, we set horizontal conductivity of the aquitard to a tiny value.

k = idomain * xr.DataArray([10.0, 10.0], {"layer": layer}, ("layer",))
k33 = idomain * xr.DataArray([100.0, 100.0], {"layer": layer}, ("layer",))

gwf_model = imod.mf6.GroundwaterFlowModel()
gwf_model["disu"] = imod.mf6.LowLevelUnstructuredDiscretization.from_dis(
    top=top, bottom=bottom, idomain=idomain.astype(np.int32)
)
# %%
gwf_model["drn"] = imod.mf6.Drainage(
    elevation=elevation,
    conductance=conductance,
    print_input=True,
    print_flows=True,
    save_flows=True,
).to_disu()
gwf_model["ic"] = imod.mf6.InitialConditions(head=45.0)
gwf_model["npf"] = imod.mf6.NodePropertyFlow(
    icelltype=icelltype.astype(np.int32),
    k=k,
    k33=k33,
).to_disu()
gwf_model["sto"] = imod.mf6.SpecificStorage(
    specific_storage=1.0e-5,
    specific_yield=0.15,
    transient=False,
    convertible=0,
)
gwf_model["oc"] = imod.mf6.OutputControl(save_head="all", save_budget="all")
gwf_model["rch"] = imod.mf6.Recharge(recharge).to_disu()

# Attach it to a simulation
simulation = imod.mf6.Modflow6Simulation("dupuit")
simulation["dupuit"] = gwf_model
# Define solver settings
simulation["solver"] = imod.mf6.Solution(
    print_option="summary",
    csv_output=False,
    no_ptc=True,
    outer_dvclose=1.0e-4,
    outer_maximum=500,
    under_relaxation=None,
    inner_dvclose=1.0e-4,
    inner_rclose=0.001,
    inner_maximum=100,
    linear_acceleration="cg",
    scaling_method=None,
    reordering_method=None,
    relaxation_factor=0.97,
)
simulation.time_discretization(["2020-01-01", "2020-01-02"])

path_dupuit = imod.util.temporary_directory()
simulation.write(path_dupuit)

# %%
# We run this simulation with the adapted Dupuit Forchheimer logic.

with DupuitForchheimerSimulation(path_dupuit, "dupuit") as xmi_sim:
    xmi_sim.run()

# %%
# Opening the results is more complicated this time around. The head results
# are written in DISU format and so they cannot be read with the standard
# function for structured MODFLOW6 output. Instead, we have to read the file
# with DISU logic and reshape the results to a structured form afterwards;
# ``imod`` provides a ``reshape`` utility to do this. 

ncell = np.product(shape)
path = path_dupuit / "dupuit/dupuit.hds"
disu_coords = {"node": np.arange(1, ncell + 1)}
d = {"ncell": ncell, "name": "head", "coords": disu_coords}
head = imod.mf6.out.disu.open_hds(path, d, True)
timecoords = {"time": head["time"].values, **coords}
head_dupuit = imod.util.reshape(head, timecoords)

# %%
# Finally, we can plot the results and observe that the simulated heads of
# the upper and lower aquifer in the reduced model are nearly identical to
# those of the fully 3D model.

head_dupuit.isel(time=0, y=75).plot.line(hue="layer")

# %%